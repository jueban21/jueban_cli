'use strict';

// 根据环境变量级别打印级别
const log = require('npmlog')
// 判断debug模式
log.level = process.env.LOG_LEVEL ? process.env.LOG_LEVEL : 'info'
// 修改头部
log.heading = "jueban"
log.headingStyle = { fg: "brightGreen", bg:"blue", bold: "bold" }

// 自定义
log.addLevel('success', 2000, { fg: "green", bold: "bold" })


module.exports = log;