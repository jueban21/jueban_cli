'use strict';
const axios = require('axios')
const log = require("@jueban-cli/log")
const urlJoin = require('url-join')
const semver = require('semver')

function getNpmInfo(npmName, registry) {
  if (!npmName) return null;
  const _registry = registry || getDefaultRegistry()
  const requestUrl = urlJoin(_registry, npmName)
  log.verbose("getNpmInfo", requestUrl)
  return axios.get(requestUrl).then(res => {
    if (res.status === 200) {
      return res.data
    }
    return null
  }).catch(err => {
    log.error("getNpmInfo", "请求异常")
    // log.verbose('getNpmInfo', err)
    // return Promise.reject(err)
  })
}

//默认仓库地址
function getDefaultRegistry(isOriginal = false) {
  return isOriginal ? "https://registry.npmjs.org" : "https://registry.npmmirror.com/"
}

async function getNpmVersions(npmName, registry) {
  let data = await getNpmInfo(npmName, registry);
  if (!data) return []
  return Object.keys(data.versions)
}

function getSemverVersions(baseVersion, versions) {
  return versions.filter(version => semver.satisfies(version, `^${baseVersion}`)).sort((a, b) => semver.gt(b, a))
}

async function getNpmSemverVersions(npmName, baseVersion, registry) {
  const versions = await getNpmVersions(npmName, registry);
  const semverVersions = await getSemverVersions(baseVersion, versions);
  log.verbose("semverVersions", semverVersions)
  if(semverVersions && semverVersions.length > 0) {
    return semverVersions[0]
  }
}

module.exports = {
  getNpmSemverVersions
}