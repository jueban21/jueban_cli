#! /usr/bin/env node

const importLocal = require("import-local")

// console.log("__filename", __filename, importLocal(__filename))
// console.log("process", process.argv)
if (importLocal(__filename)) {
    require("npmlog").info("cli", "正在使用jueban-cli 本地版本")
} else {
    require("../lib/index")(process.argv.slice(2))
}