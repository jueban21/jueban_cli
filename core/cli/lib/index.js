'use strict';

module.exports = core;
const path = require('path')

const semver = require('semver')
const colors = require('colors/safe')
const rootCheck = require('root-check')
const userhome = require("userhome")()
const pathExists = require('path-exists')
// const minimist = require("minimist")
const dotenv = require('dotenv');
const commander = require('commander')
const { getNpmSemverVersions } = require('@jueban-cli/get-npm-info')

const pkg = require("../package.json");
const log = require("@jueban-cli/log");
const constant = require("./const");

let args;
const program = new commander.Command()

async function core() {
  try {
    checkPkgVersion()
    checkNodeVersion()
    checkRoot()
    checkUserHome()
    // checkInputArgs()
    checkEnv()
    await checkGlobalUpdate()
    registerCommand()
  } catch (error) {
    log.error(error?.message)
  }
}

// 检查版本号
function checkPkgVersion() {
  log.notice('cli', pkg.version)
}

// 检查用户node版本号
function checkNodeVersion() {
  // 拿到node版本号
  const currentVersion = process.version;
  const { LOWEST_NODE_VERSION } = constant

  if (!semver.gte(currentVersion, LOWEST_NODE_VERSION)) {
    throw new Error(colors.red(`jueban-cli 需要安装 ${LOWEST_NODE_VERSION}以上版本的node`))
  }
}

// 检查用户权限问题，如果是管理员权限那么会降级权限
function checkRoot() {
  rootCheck()
  // if (process.geteuid) {
  //   console.log(`Current uid: ${process.geteuid()}`);
  // }
}

// 检查用户的主目录权限
function checkUserHome() {
  if (userhome && !pathExists.sync(userhome)) {
    throw new Error(colors.red("当前登录用户主目录不存在！"))
  }
}

//检查入参
// function checkInputArgs() {
//   args = minimist(process.argv.slice(2))
//   checkArgs()
// }


// 检查环境环境变量文件，并且自动写入process.env
function checkEnv() {
  const dotenvPath = path.resolve(userhome, '.env');
  if (pathExists.sync(dotenvPath)) {
    log.verbose("用户配置了环境变量文件")
    dotenv.config({
      path: dotenvPath
    })
  }

  createDefaultConfig()
  log.verbose('process.env', process.env.CLI_HOME_PATH)
}

// 创建默认环境变量
function createDefaultConfig() {
  const cliConfig = {
    home: userhome,
  }
  if (process.env.CLI_HOME) {
    cliConfig['CLI_HOME_PATH'] = path.join(userhome, process.env.CLI_HOME)
  } else {
    cliConfig['CLI_HOME_PATH'] = path.join(userhome, constant.DEFAULT_CLI_HOME)
  }
  process.env.CLI_HOME_PATH = cliConfig.CLI_HOME_PATH
}


// 检查是否要全局更新
async function checkGlobalUpdate() {
  // 1、获取当前版本号和模块名称
  const currentVersion = pkg.version
  const npmName = pkg.name

  // 2、调用npm API获取线上的版本号; 提取所有版本号，对比哪些版本号大于当前版本号
  const lastVersion = await getNpmSemverVersions(npmName, currentVersion);
  log.verbose("lastVersion", lastVersion)

  // 3、获取最新版本号，提示用户更新到最新版本3
  if (lastVersion && semver.gt(lastVersion, currentVersion)) {
    log.warn("更新提示", colors.yellow(`请手动更新${npmName},当前版本：${currentVersion}, 最新版本：${lastVersion}
    更新命令 npm install -g ${npmName}`))
  }
}


// 注册脚手架命令
function registerCommand() {
  program
    .name(Object.keys(pkg.bin)[0])
    .usage('<command> [options]')
    .version(pkg.version)
    .option('-d, --debug', "是否开启调试模式", false)

  program
    .command('init [projectName]')
    .option('-f --force', '是否强制初始化项目')
    .action((projectName, cmdObj) => {
      console.log('init', projectName, cmdObj.force);
    })

  // 监听脚手架option：debug参数，
  program.on('option:debug', function () {
    if (program.opts()?.debug) {
      process.env.LOG_LEVEL = 'verbose'
    } else {
      process.env.LOG_LEVEL = 'info'
    }
    log.level = process.env.LOG_LEVEL
  })

  // 对未知命令的监听
  program.on('command:*', function (obj) {
    const availableCommands = program.commands.map(cmd => cmd.name());
    log.warn('[未知的命令]:', colors.red(obj[0]))
    if (availableCommands.length > 0) {
      log.info('[可用命令]', availableCommands.join(","));
    }
  })

  //解析参数
  program.parse(process.argv)

  if (program.args && program.args.length < 1) {
    // 没有命令输出的时候，打印出帮助文档 
    program.outputHelp()
    console.log();
  }
}
